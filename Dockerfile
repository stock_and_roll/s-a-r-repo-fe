FROM nginxinc/nginx-unprivileged:latest

EXPOSE 80

COPY dist/s-a-r-fe/* /usr/share/nginx/html/
