import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PriceFeedComponent } from 'src/app/price-feed/price-feed.component';
import { StrategiesComponent } from './strategies/strategies.component';
import { ActivityComponent } from './activity/activity.component';
import { NewStrategyComponent } from './new-strategy/new-strategy.component';
import { TradeListComponent } from './trade-list/trade-list.component';
 
const routes: Routes = [
  { path: 'price-feed', component: PriceFeedComponent},
  { path: '', redirectTo: '/price-feed', pathMatch: 'full' },
  { path: 'strategies', component: StrategiesComponent},
  { path: '', redirectTo: '/strategies', pathMatch: 'full' },
  { path: 'activity', component: ActivityComponent},
  { path: '', redirectTo: '/activity', pathMatch: 'full' },
  { path: 'new-strategy', component: NewStrategyComponent},
  { path: '', redirectTo: '/new-strategy', pathMatch: 'full' },
  { path: 'trade-list', component: TradeListComponent},
  { path: '', redirectTo: '/trade-list', pathMatch: 'full' }
];
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
