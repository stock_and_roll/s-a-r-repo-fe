export class PriceFeed {
  ticker: string;
  lastTradeNumValue: number
  lastFiveTrades: string[];
  cumulativeVolume: number;
  dayHigh: number;
  dayLow: number;

  constructor(ticker, lastTradeNumValue, lastFiveTrades, cumulativeVolume, dayHigh, dayLow){
    this.ticker = ticker;
    this.lastTradeNumValue = lastTradeNumValue;
    this.lastFiveTrades = lastFiveTrades;
    this.cumulativeVolume = cumulativeVolume;
    this.dayHigh = dayHigh;
    this.dayLow = dayLow;
  }
}

