export class Stock {
    id: number;
    ticker: string;
  
    constructor(id, ticker){
        this.id = id;
        this.ticker = ticker;
    }
  }
  
  