import { Stock } from './stock';
import { StockService } from '../service/stock.service';

export class Strategy {
  currentPosition: number;
  exitProfitLoss: number;
  id: number;
  lastTradePrice: number;
  profit: number;
  size: number;
  stock: Stock = new Stock(0,"")
  stopped: Date;

    constructor(currentPosition, exitProfitLoss, id, lastTradePrice, 
      profit, size, stock, stopped){
      this.currentPosition = currentPosition;
      this.size = size;
      this.exitProfitLoss = exitProfitLoss;
      this.id = id;
      this.lastTradePrice = lastTradePrice;
      this.profit = profit;
      this.stopped = stopped;
      this.stock = stock;
    }
  }