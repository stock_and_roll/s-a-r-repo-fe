import { Observable, timer, Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Strategy } from '../model/strategy';
import { StrategyService } from '../service/strategy.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors, FormControl } from '@angular/forms';
import { StockService } from '../service/stock.service';
import { Stock } from '../model/stock';

export const atLeastOne = (validator: ValidatorFn, controls:string[] = null) => (
  group: FormGroup,
): ValidationErrors | null => {
  if(!controls){
    controls = Object.keys(group.controls)
  }

  const hasAtLeastOne = group && group.controls && controls
    .some(k => !validator(group.controls[k]));

  return hasAtLeastOne ? null : {
    atLeastOne: true,
  };
};

@Component({
  selector: 'app-new-strategy',
  templateUrl: './new-strategy.component.html',
  styleUrls: ['./new-strategy.component.css']
})
export class NewStrategyComponent implements OnInit {

  private updateSubscription: Subscription;

  private strategy = new Strategy(1, 100, 100, 0, 250, 100, new Stock(1,""),new Date());

  newStrategyForm: FormGroup;

  stocks: Stock[];
  stockId: number;
  stock: Stock = new Stock(0,"");
  size: number;
  profit: number;

  constructor(private strategyService: StrategyService, private formBuilder: FormBuilder, private stockService: StockService) { }

  ngOnInit() {
    this.getStocks();
    this.createForm();
  }

  onSubmit() {
    
    for (var i=0; i < this.stocks.length; i++) {
      if (this.stocks[i].ticker == this.newStrategyForm.value.stockName) {
          this.stockId = this.stocks[i].id;
      }
  }
    this.stock.id = this.stockId;
    this.stock.ticker = this.newStrategyForm.value.stockName;
    this.size = this.newStrategyForm.value.size;
    this.profit = this.newStrategyForm.value.profit;
    this.strategyService.createStrategy(this.stock, this.size, this.profit).subscribe(data => {
      console.log(data);
    });

    location.reload();
  }

  private createForm() {
    this.newStrategyForm = this.formBuilder.group({
      'stockName': [null, [Validators.required]],
      'strategyType': [null, [Validators.required]],
      'size': [null, [Validators.required, Validators.min(10), Validators.max(10000), Validators.pattern("^[0-9]*$")]],
      'profit': [null, [Validators.pattern("^[0-9]*$"), Validators.min(10), Validators.max(10000),Validators.required]]
     });
  }

  getErrorStrategyType() {
    return this.newStrategyForm.get('strategyType').hasError('required') ? 'Field is required' : '';
  }

  getErrorSize() {
    return this.newStrategyForm.get('size').hasError('required') ? 'Field is required' :
      this.newStrategyForm.get('size').hasError('pattern') ? 'Invalid number entry' : 
      this.newStrategyForm.get('size').hasError('min') ? 'The size value is too small' : 
      this.newStrategyForm.get('size').hasError('max') ? 'The size value is too large' : '';
  }

  getErrorStockName() {
    return this.newStrategyForm.get('stockName').hasError('required') ? 'Field is required' : '';
  }

  getErrorProfit() {
    return this.newStrategyForm.get('profit').hasError('pattern') ? 'Invalid value entered' :
    this.newStrategyForm.get('profit').hasError('min') ? 'The profit value is too small' : 
    this.newStrategyForm.get('profit').hasError('max') ? 'The profit value is too large' :
    this.newStrategyForm.get('profit').hasError('required') ? 'Field is required' : '';
  }

  getStocks() {
    this.stockService.findAll().subscribe(data => {
      this.stocks = data.reverse();
    });
  }

}
