import { Observable, timer, Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { PriceFeed } from '../model/price-feed';
import { PriceFeedService } from '../service/price-feed.service';

export interface PeriodicElement {
  ticker: string;
  lastTradeNumValue: number
  lastFiveTrades: string[];
  cumulativeVolume: number;
  dayHigh: number;
  dayLow: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {ticker: 'AAPL', lastTradeNumValue: 130,
  lastFiveTrades: ["assets/downarrow_burned.png",
  "assets/downarrow_burned.png",
  "assets/downarrow_burned.png",
  "assets/downarrow_burned.png",
  "assets/downarrow_burned.png"], 
  cumulativeVolume: 100, dayHigh: 300.0, dayLow: 100.0},
  {ticker: 'GOOG', lastTradeNumValue: 230,
  lastFiveTrades: ["assets/arrowup_burned.png",
  "assets/downarrow_burned.png",
  "assets/arrowup_burned.png",
  "assets/downarrow_burned.png",
  "assets/arrowup_burned.png"], 
  cumulativeVolume: 200, dayHigh: 400.0, dayLow: 200.0},
  {ticker: 'BRK-A', lastTradeNumValue: 330,
  lastFiveTrades: ["assets/downarrow_burned.png",
  "assets/arrowup_burned.png",
  "assets/arrowup_burned.png",
  "assets/downarrow_burned.png",
  "assets/downarrow_burned.png"], 
  cumulativeVolume: 300, dayHigh: 500.0, dayLow: 300.0},
  {ticker: 'NSC', lastTradeNumValue: 430,
  lastFiveTrades: ["assets/arrowup_burned.png",
  "assets/downarrow_burned.png",
  "assets/arrowup_burned.png",
  "assets/arrowup_burned.png",
  "assets/arrowup_burned.png"], 
  cumulativeVolume: 400, dayHigh: 600.0, dayLow: 400.0},
  {ticker: 'MSFT', lastTradeNumValue: 530,
  lastFiveTrades: ["assets/downarrow_burned.png",
  "assets/downarrow_burned.png",
  "assets/downarrow_burned.png",
  "assets/downarrow_burned.png",
  "assets/arrowup_burned.png"], 
  cumulativeVolume: 500, dayHigh: 700.0, dayLow: 500.0}
];

@Component({
  selector: 'app-price-feed',
  templateUrl: './price-feed.component.html',
  styleUrls: ['./price-feed.component.css']
})
export class PriceFeedComponent implements OnInit {

  private updateSubscription: Subscription;

  priceFeed: PriceFeed[];

  constructor(private priceFeedService: PriceFeedService) { }

  displayedColumns: string[] = ['ticker', 'lastTradeNumValue', 'lastFiveTrades', 'cumulativeVolume',
  'dayHigh', 'dayLow'];
  dataSource = ELEMENT_DATA;

  ngOnInit() {
    this.updatePriceFeed();
  }

  private updatePriceFeed() {
  this.priceFeedService.findAll().subscribe(data => {
      this.priceFeed = data.reverse();
      console.log("loading price-feed: ");
      console.log(this.priceFeed);
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(10000).subscribe(
      (val) => { this.updatePriceFeed() }
    );
  }
}
