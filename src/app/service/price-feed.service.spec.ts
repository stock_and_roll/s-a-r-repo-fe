import { TestBed } from '@angular/core/testing';

import { PriceFeedService } from './price-feed.service';

describe('PriceFeedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PriceFeedService = TestBed.get(PriceFeedService);
    expect(service).toBeTruthy();
  });
});
