import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PriceFeed } from '../model/price-feed';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PriceFeedService {

  private priceFeedUrl: string;

  constructor(private http: HttpClient) {
    this.priceFeedUrl = environment.rest_host + '/price-feed';
  }
 
  public findAll(): Observable<PriceFeed[]> {
    return this.http.get<PriceFeed[]>(this.priceFeedUrl);
  }
}
