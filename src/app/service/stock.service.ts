import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Stock } from '../model/stock';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  private stockUrl: string;

  constructor(private http: HttpClient) {
    this.stockUrl = environment.rest_host + '/stock';
  }
 
  public findAll(): Observable<Stock[]> {
    return this.http.get<Stock[]>(this.stockUrl);
  }
}
