import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Strategy } from '../model/strategy';
import { Stock } from '../model/stock';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class StrategyService {

  private strategyUrl: string;

  constructor(private http: HttpClient) {
    this.strategyUrl = environment.rest_host + '/strategy';
  }

  createStrategy(stock: Stock, size: number, exitProfitLoss: number) {
    let j = {
      "id": -1,
      "stock": stock,
      "size": size,
      "exitProfitLoss": exitProfitLoss
    }
    //var myJSON = JSON.stringify(j);
    return this.http.post(this.strategyUrl, j, httpOptions);
  }

  public findAll(): Observable<Strategy[]> {
    return this.http.get<Strategy[]>(this.strategyUrl);
  }

  public setActive(strategyName: string){
    return this.http.put(this.strategyUrl, strategyName);
  }
}
