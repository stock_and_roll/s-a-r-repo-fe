import { Component, OnInit } from '@angular/core';
import { timer, Subscription } from 'rxjs';
import { Strategy } from '../model/strategy';
import { StrategyService } from '../service/strategy.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-strategies',
  templateUrl: './strategies.component.html',
  styleUrls: ['./strategies.component.css']
})
export class StrategiesComponent implements OnInit {

  private updateSubscription: Subscription;
  
  strategies: Strategy[];

  displayedColumns: string[] = ['currentPosition', 'exitProfitLoss', 'id', 'lastTradePrice',
  'profit', 'size','stock.ticker','stopped'];
  
  public dataSource = new MatTableDataSource<Strategy>();

  constructor(private strategiesService: StrategyService) { }

  ngOnInit() {
    this.updateStrategies();
    this.dataSource = new MatTableDataSource(this.strategies);
  }

  private updateStrategies() {
    this.strategiesService.findAll().subscribe(data => {
        this.strategies = data.reverse();
        this.dataSource.data = data as Strategy[];
        console.log("loading strategy: ");
        console.log(this.strategies);
        this.startRefreshTimer();
      });
    }
  
    private startRefreshTimer() {
      this.updateSubscription = timer(10000).subscribe(
        (val) => { this.updateStrategies() }
      );
    }

}
