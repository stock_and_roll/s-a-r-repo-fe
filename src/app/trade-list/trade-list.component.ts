import { Observable, timer, Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Trade } from '../model/trade';
import { TradeService } from '../service/trade.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-trade-list',
  templateUrl: './trade-list.component.html',
  styleUrls: ['./trade-list.component.css']
})
export class TradeListComponent implements OnInit {

  private updateSubscription: Subscription;

  trades: Trade[];

  displayedColumns: string[] = ['id', 'stockTicker', 'tradeType', 'price',
  'size', 'lastStateChange'];
  
  public dataSource = new MatTableDataSource<Trade>();

  constructor(private tradeService: TradeService) { }

  ngOnInit() {
    this.updateTrades();
    this.dataSource = new MatTableDataSource(this.trades);
  }

  private updateTrades() {
  this.tradeService.findAll().subscribe(data => {
      this.trades = data.reverse();
      this.dataSource.data = data as Trade[];
      console.log("loading trades: ");
      console.log(this.trades);
      this.startRefreshTimer();
    });
  }

  private startRefreshTimer() {
    this.updateSubscription = timer(10000).subscribe(
      (val) => { this.updateTrades() }
    );
  }
}
